const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two functions together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => {
    return a + b;
  } 
// Path to add function
app.get('/add/', (req, res) => {
  const x = add(1,2);
  res.send(`Sum: ${x}`);
  })

  // Welcome msg
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})