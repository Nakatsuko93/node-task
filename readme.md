# Node Task t

**I don't really know what to type here... maybe I should introduce myself or something**

```
  First name(s): Jenni Josefiina.
  Last name: Grönberg.
  Age: 27 years old.
  Family: Mother, father, brother, stepfather and my cat.
```

## Top 3 hobbies:

1. Reading
2. Cooking
3. Playing with my cat lol


## Top 3 life goals right now:
- [ ] To graduate on time
- [ ] Find a better job
- [ ] Be happy :)



![alt text for test image](https://baap.ponet.fi/share/mosquitto_broker.png "text")

*Note: I would have liked to put some nice cat pictures here but I was worried about the copyrights so I ended up using teachers example picture.*